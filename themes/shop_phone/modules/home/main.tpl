<!-- BEGIN: main -->
<link rel="stylesheet" type="text/css" media="screen" href="{NV_BASE_SITEURL}themes/{THEMES}/css/shops.css" />
<div>
<!-- BEGIN: catalogs -->
<div class="panel">
    <div class="panel-home">
			<h2><a class="pull-left" href="{LINK_CATALOG}" title="{TITLE_CATALOG}">{TITLE_CATALOG}</a></h2>
			<span class="pull-right">
			<!-- BEGIN: subcatloop -->
			<a href="{SUBCAT.link}" title="{SUBCAT.title}">{SUBCAT.title}</a>&nbsp;&nbsp;&nbsp;
			<!-- END: subcatloop -->
			</span>
			<div class="clear"></div>
		</div>
    <div id="mySliderTabs">
        <div id="homepro">
            <div class="row padding-row">
                <div class="col-md-24 panel-body-main" style="padding: 0">
                    <!-- BEGIN: loophomepro -->
                    <div class="col-md-6 col-sm-12 col-xs-24 margin-col-news">
                        <div class="thumbnail padding-thum">
                        	<div style="height: {height}px" class="padding-product">
                        		<a href="{link}" title="{title}"><img src="{src_img}" alt="{title}" class="img-thumbnail" style="max-height:{height}px;max-width:{width}px;"/></a>
                        	</div>
                        	<div class="info_pro">
								<!-- BEGIN: img_hot -->
								<div class="label_product_new">
            						<span class="label_sale_news"></span>
            					</div>
								<!-- END: img_hot -->
								<!-- BEGIN: discounts -->
								<div class="label_product">
									<span class="label label_sale">{PRICE.discount_percent}{PRICE.discount_unit}</span>
								</div>
								<!-- END: discounts -->
							</div>
							<div class="caption text-center">
								<h3 class="product-name"><a href="{link}" title="{title}">{TITLE0}</a></h3>
								<!-- BEGIN: price -->
                                <!-- BEGIN: nocontactprice -->
                                <!-- BEGIN: newprice -->
                                <p class="price price-box text-center">
                                	<span class="money product-price">{PRICE.sale_format}<span style="font-size: 9px">{PRICE.unit}</span></span>
                                </p> 
                                <!-- END: newprice --> 
                                <!-- BEGIN: no_discounts -->
                                <p class="price price-box text-center">
                                	<span class="money product-price">{PRICE.sale_format}<span style="font-size: 9px">{PRICE.unit}</span></span>
                                </p> 
                                <!-- END: no_discounts -->
                                <!-- END: nocontactprice -->
                                <!-- BEGIN: contactprice -->
                                <p class="price price-box text-center">
                                	<span class="money product-price">{LANG.price_contact}</span>
                                </p>
                                <!-- END: contactprice -->
                                <!-- END: price -->
							</div>
							<div class="clearfix product-box">
								<a href="{link}" title="{title}" class="button dark">Xem chi tiết</a>
							</div>
                        </div>
                    </div>
                    <!-- END: loophomepro -->
                </div>
            </div>
        </div>
    </div>
</div>
[BLOCK_BANNER_{i}]
<!-- END: catalogs -->
</div>
<script type="text/javascript">
$(window).on('load', function() {	
	$.each( $('.margin-col-news .thumbnail'), function(k,v){
		var height1 = $($('.margin-col-news .thumbnail')[k]).height();
		var height2 = $($('.margin-col-news .thumbnail')[k+1]).height();
		var height = ( height1 > height2 ? height1 : height2 );
		$($('.margin-col-news .thumbnail')[k]).height( height );
		$($('.margin-col-news .thumbnail')[k+1]).height( height );
	});
});
</script>
<!-- END: main -->