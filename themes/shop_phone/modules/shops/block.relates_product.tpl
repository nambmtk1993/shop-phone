<!-- BEGIN: main -->
<div class="row margin-catalogy">
	<div class="text-center css-title-block">{DATA.title}</div>
	<!-- BEGIN: loop -->
	<div class="clearfix col-md-6 col-xs-24 col-sm-12 margin-col">
		<div class="product-box">
			<div class="product-thumbnail">
				<a href="{link}" title="{title}"><img src="{src_img}" alt="{title}" class="img-thumbnail" width="{WIDTH}" /></a> <br /> 
			</div>
			<div class="product-info">
				<h3 class="product-name"><a href="{link}" title="{title}">{title}</a></h3>
			</div>
			<div class="price-box clearfix">
				<!-- BEGIN: price -->
				<span class="price"> <!-- BEGIN: discounts --> <span class="money f-left product-price show">{PRICE.sale_format} {PRICE.unit}<span style="font-size: 9px">{money_unit}</span></span> <span class="discounts_money show product-price-old">{PRICE.price_format} {PRICE.unit}</span> <!-- END: discounts --> <!-- BEGIN: no_discounts --> <span class="money product-price show">{PRICE.price_format} {PRICE.unit} <span style="font-size: 9px">{money_unit}</span></span> <!-- END: no_discounts --></span>
				<!-- END: price -->
				<!-- BEGIN: contact -->
				<span class="price"><span class="money product-price show">{LANG.price_contact}</span></span>
				<!-- END: contact -->
			</div>
			<div>
				<a class="button dark" href="{link}">Xem chi tiết</a>
			</div>
		</div>
	</div>
	<!-- END: loop -->
</div>
<script type="text/javascript">
$(window).on('load', function() {	
	$.each( $('.margin-col .product-box'), function(k,v){
		var height1 = $($('.margin-col .product-box')[k]).height();
		var height2 = $($('.margin-col .product-box')[k+1]).height();
		var height = ( height1 > height2 ? height1 : height2 );
		$($('.margin-col .product-box')[k]).height( height );
		$($('.margin-col .product-box')[k+1]).height( height );
	});
});
</script>
<!-- END: main -->