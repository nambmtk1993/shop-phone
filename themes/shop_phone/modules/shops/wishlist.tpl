<!-- BEGIN: main -->
<div id="products" class="clearfix">
    <!-- BEGIN: items -->
    <div class="col-sm-12 col-md-{num} margin-col-news" id="item_{ID}">
        <div class="thumbnail">
            <div style="height: {height}px">
                <a href="{LINK}" title="{TITLE}"><img src="{IMG_SRC}" alt="{TITLE}" data-content='{hometext}' class="img-thumbnail" style="max-width:{width}px; max-height:{height}px"></a>
            </div>
            <div class="info_pro">
            	<!-- BEGIN: new -->
            	<div class="label_product_new">
            		<span class="label_sale_news"></span>
            	</div>
            	<!-- END: new -->
            	<!-- BEGIN: discounts -->
            	<div class="label_product">
            		<span class="label label_sale">{PRICE.discount_percent}{PRICE.discount_unit}</span>
            	</div>
            	<!-- END: discounts -->
            	<!-- BEGIN: point -->
            	<span class="label label-info" title="{point_note}">+{point}</span>
            	<!-- END: point -->
            	<!-- BEGIN: gift -->
            	<span class="label label-success">+<em class="fa fa-gift fa-lg">&nbsp;</em></span>
            	<!-- END: gift -->
            </div>
            <div class="caption text-center">
                <h3 class="product-name"><a href="{LINK}" title="{TITLE}">{TITLE0}</a></h3>
                
                <!-- BEGIN: product_code -->
                <p class="label label-default hidden">{PRODUCT_CODE}</p>
                <!-- END: product_code -->

                <!-- BEGIN: adminlink -->
                <p>{ADMINLINK}</p>
                <!-- END: adminlink -->

				<!-- BEGIN: price -->
				<p class="price price-box text-center">
                    <!-- BEGIN: discounts -->
                    <span class="money product-price">{PRICE.sale_format}<span style="font-size: 9px">{PRICE.unit}</span> <span class="discounts_money product-price-old">{PRICE.price_format}</span></span>
                    <!-- END: discounts -->

					<!-- BEGIN: no_discounts -->
					<span class="money product-price">{PRICE.price_format}<span style="font-size: 9px">{PRICE.unit}</span></span>
					<!-- END: no_discounts -->
				</p>
				<!-- END: price -->

                <!-- BEGIN: contact -->
                <p class="price price-box"><span class="money product-price">{LANG.price_contact}</span></p>
                <!-- END: contact -->

                <!-- BEGIN: compare -->
                <p><input type="checkbox" value="{ID}"{ch} onclick="nv_compare({ID});" id="compare_{ID}"/><a href="#" onclick="nv_compare_click();" >&nbsp;{LANG.compare}</a></p>
                <!-- END: compare -->

                <div class="clearfix actions-button">
                    <!-- BEGIN: order -->
                    <a href="{LINK}" id="{ID}" title="{LANG.add_product}"><button type="button" class="btn btn-primary btn-cart btn-xs"><em class="fa fa-lg fa-shopping-bag"></em></button></a>
                    <!-- END: order -->
                	<a href="javascript:void(0)" title="{LANG.wishlist_del_item}" ><button type="button" onclick="wishlist_del_item({ID})" class="btn btn-primary btn-xs btn-cart"><em class="fa fa-trash-o fa-lg"></em></button></a>
                </div>
            </div>
        </div>
    </div>
    <!-- END: items -->
</div>

<!-- BEGIN: modal_loaded -->
<div class="modal fade" id="idmodals" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">{LANG.add_product}</h4>
			</div>
			<div class="modal-body">
				<em class="fa fa-spinner fa-spin">&nbsp;</em>
			</div>
		</div>
	</div>
</div>
<!-- END: modal_loaded -->

<!-- BEGIN: pages -->
<div class="text-center">
    {generate_page}
</div>
<!-- END: pages -->
<div class="msgshow" id="msgshow">&nbsp;</div>

<script type="text/javascript" data-show="after">
	var lang_del_confirm = '{LANG.wishlist_del_item_confirm}';
</script>

<!-- BEGIN: tooltip_js -->
<script type="text/javascript" data-show="after">
	$(document).ready(function() {$("[data-rel='tooltip']").tooltip({
		placement: "bottom",
		html: true,
		title: function(){return '<p class="text-justify">' + $(this).data('content') + '</p><div class="clearfix"></div>';}
	});});
</script>
<!-- END: tooltip_js -->
<!-- END: main -->