<!-- BEGIN: main -->
<div class="clearfix carthome">
    <!-- BEGIN: enable -->
    <div class="col-xs-24">
        <div class="notify">{num}</div>
        <div class="text-center"><a title="{LANG.cart_check_out}" href="{LINK_VIEW}" id="submit_send"><i class="fa fa-lg fa-shopping-bag" aria-hidden="true"></i></a></div>
    </div>	
	<!-- END: enable -->
    <div class="cart-block">
        <p class="clearfix">
            <a title="{LANG.cart_check_out}" href="{LINK_VIEW}" id="submit_send">{LANG.cart_check_out}</a>
        </p>
        <!-- BEGIN: point -->
        <p class="clearfix">
            <a title="{LANG.point_cart_text}" href="{POINT_URL}">{LANG.point_cart_text}</a> ({POINT})
        </p>
        <!-- END: point -->
    
        <!-- BEGIN: wishlist -->
        <p class="clearfix">
            <a title="{LANG.wishlist_product}" href="{WISHLIST}">{LANG.wishlist_product}</a> (<span id="wishlist_num_id">{NUM_ID}</span>)
        </p>
        <!-- END: wishlist -->
    
        <!--  BEGIN: history -->
        <p>
            <a href="{LINK_HIS}"><span>{LANG.history_title}</span></a>
        </p>
        <!--  END: history -->
    
        <!-- BEGIN: disable -->
        <p>
            {LANG.active_order_dis}
        </p>
        <!-- END: disable -->
    </div>
</div>
<!-- END: main -->