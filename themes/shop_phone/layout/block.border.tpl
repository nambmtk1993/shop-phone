<!-- BEGIN: mainblock -->
<div class="panel panel-primary">
	<div class="panel-heading">
		{BLOCK_TITLE}
	</div>
	<div class="well no-border">
		{BLOCK_CONTENT}
	</div>
</div>
<!--  END: mainblock -->