<!-- BEGIN: main -->
{FILE "header_only.tpl"}
{FILE "header_extended.tpl"}
<div class="row">
	[HEADER]
</div>
<div class="row">
    <div class="col-sm-17 col-md-18 col-sm-push-7 col-md-push-6 padd-left-col">
        [TOP]
        {MODULE_CONTENT}
        [BOTTOM]
    </div>
	<div class="col-sm-7 col-md-6 col-sm-pull-17 col-md-pull-18 padd-right-col">
		[LEFT]
	</div>
</div>
<div class="row">
	[FOOTER]
</div>
{FILE "footer_extended.tpl"}
{FILE "footer_only.tpl"}
<!-- END: main -->