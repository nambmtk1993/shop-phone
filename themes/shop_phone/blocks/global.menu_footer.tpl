<!-- BEGIN: main -->
<!-- BEGIN: top_menu -->
<div class="col-md-6 col-xs-24 col-sm-12">
	<aside>
		<header>
			<h4>{TOP_MENU.title_trim}</h4>
		</header>
		<ul class="list-links">
			<!-- BEGIN: sub -->
            <!-- BEGIN: item -->
            <li>
            	<a href="{SUB.link}">{SUB.title_trim}</a>
            </li>
            <!-- END: item -->
            <!-- END: sub -->
		</ul>
	</aside>
</div>
<!-- END: top_menu -->
<!-- END: main -->