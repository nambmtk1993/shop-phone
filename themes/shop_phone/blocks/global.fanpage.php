<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2017 VINADES.,JSC. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate May 17, 2016 11:34:27 AM
 */

if( ! defined( 'NV_MAINFILE' ) ) die( 'Stop!!!' );

if( ! nv_function_exists( 'nv_fanpage_topbar' ) )
{
	function nv_fanpage_config( $module, $data_block, $lang_block )
	{
		global $site_mods;

		$html = '';
		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['name'] . '</td>';
		$html .= '	<td><input type="text" name="config_name" class="form-control w300" value="' . $data_block['name'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['href'] . '</td>';
		$html .= '	<td><input type="text" name="config_href" class="form-control w300" value="' . $data_block['href'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['width'] . '</td>';
		$html .= '	<td><input type="text" name="config_width" class="form-control w300" value="' . $data_block['width'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['height'] . '</td>';
		$html .= '	<td><input type="text" name="config_height" class="form-control w300" value="' . $data_block['height'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['tabs'] . '</td>';
		$html .= '	<td><input type="text" name="config_tabs" class="form-control w300" value="' . $data_block['tabs'] . '"/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['hide_cover'] . '</td>';
		$checked = ( $data_block['hide_cover'] == 1 ) ? 'checked="checked"' : '';
		$html .= '	<td><input type="checkbox" name="config_hide_cover" class="form-control" value="1" ' . $checked . '/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['show_facepile'] . '</td>';
		$checked = ( $data_block['show_facepile'] == 1 ) ? 'checked="checked"' : '';
		$html .= '	<td><input type="checkbox" name="config_show_facepile" class="form-control" value="1" ' . $checked . '/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['hide_cta'] . '</td>';
		$checked = ( $data_block['hide_cta'] == 1 ) ? 'checked="checked"' : '';
		$html .= '	<td><input type="checkbox" name="config_hide_cta" class="form-control" value="1" ' . $checked . '/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['small_header'] . '</td>';
		$checked = ( $data_block['small_header'] == 1 ) ? 'checked="checked"' : '';
		$html .= '	<td><input type="checkbox" name="config_small_header" class="form-control" value="1" ' . $checked . '/></td>';
		$html .= '</tr>';

		$html .= '<tr>';
		$html .= '	<td>' . $lang_block['adapt_container_width'] . '</td>';
		$checked = ( $data_block['adapt_container_width'] == 1 ) ? 'checked="checked"' : '';
		$html .= '	<td><input type="checkbox" name="config_adapt_container_width" class="form-control" value="1" ' . $checked . '/></td>';
		$html .= '</tr>';

		return $html;
	}

	function nv_fanpage_submit( $module, $lang_block )
	{
		global $nv_Request;
		$return = array();
		$return['error'] = array();
		$return['config']['name'] = $nv_Request->get_title( 'config_name', 'post' );
		$return['config']['href'] = $nv_Request->get_title( 'config_href', 'post' );
		$return['config']['width'] = $nv_Request->get_title( 'config_width', 'post' );
		$return['config']['height'] = $nv_Request->get_title( 'config_height', 'post' );
		$return['config']['tabs'] = $nv_Request->get_title( 'config_tabs', 'post' );
		$return['config']['hide_cover'] = $nv_Request->get_int( 'config_hide_cover', 'post', 0 );
		$return['config']['show_facepile'] = $nv_Request->get_int( 'config_show_facepile', 'post', 0 );
		$return['config']['hide_cta'] = $nv_Request->get_int( 'config_hide_cta', 'post', 0 );
		$return['config']['small_header'] = $nv_Request->get_int( 'config_small_header', 'post', 0 );
		$return['config']['adapt_container_width'] = $nv_Request->get_int( 'config_adapt_container_width', 'post', 0 );
		return $return;
	}

	/**
	 * nv_fanpage()
	 *
	 * @param mixed $block_config
	 * @return
	 */
	function nv_fanpage( $block_config )
	{
		global $global_config, $site_mods, $lang_global, $module_name, $home, $lang_block;

		if( file_exists( NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.fanpage.tpl' ) )
		{
			$block_theme = $global_config['module_theme'];
		}
		elseif( file_exists( NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.fanpage.tpl' ) )
		{
			$block_theme = $global_config['site_theme'];
		}
		else
		{
			$block_theme = 'default';
		}

		$xtpl = new XTemplate( 'global.fanpage.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks' );
		$xtpl->assign( 'LANG', $lang_global );
		$xtpl->assign( 'NV_BASE_SITEURL', NV_BASE_SITEURL );
		$xtpl->assign( 'BLOCK_THEME', $block_theme );
		$xtpl->assign( 'THEME_SITE_HREF', NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA );
		$xtpl->assign( 'TEMPLATE', $global_config['module_theme'] );

		if( $block_config['hide_cta'] == 1 )
		{
			$block_config['cta'] = 'true';
		}
		else
		{
			$block_config['cta'] = 'false';
		}

		if( $block_config['small_header'] == 1 )
		{
			$block_config['small_header'] = 'true';
		}
		else
		{
			$block_config['small_header'] = 'false';
		}

		if( $block_config['adapt_container_width'] == 1 )
		{
			$block_config['adapt_container_width'] = 'true';
		}
		else
		{
			$block_config['adapt_container_width'] = 'false';
		}

		if( $block_config['hide_cover'] == 1 )
		{
			$block_config['cover'] = 'true';
		}
		else
		{
			$block_config['cover'] = 'false';
		}

		if( $block_config['show_facepile'] == 1 )
		{
			$block_config['facepile'] = 'true';
		}
		else
		{
			$block_config['facepile'] = 'false';
		}

		$xtpl->assign('DATA', $block_config);

		if( ! empty($block_config['width']) )
		{
			$xtpl->parse( 'main.width' );
		}
		if( ! empty($block_config['height']) )
		{
			$xtpl->parse( 'main.height' );
		}
		if( ! empty($block_config['tabs']) )
		{
			$xtpl->parse( 'main.tabs' );
		}

		if( ! empty($block_config['hide_cta']) )
		{
			$xtpl->parse( 'main.cta' );
		}

		if( ! empty($block_config['small_header']) )
		{
			$xtpl->parse( 'main.small_header' );
		}
		if( ! empty($block_config['adapt_container_width']) )
		{
			$xtpl->parse( 'main.adapt_container_width' );
		}
		if( ! empty($block_config['cover']) )
		{
			$xtpl->parse( 'main.hide_cover' );
		}
		if( ! empty($block_config['facepile']) )
		{
			$xtpl->parse( 'main.show_facepile' );
		}

		if( ! empty($block_config['name']) )
		{
			$xtpl->parse( 'main.name' );
		}

		$xtpl->parse( 'main' );
		return $xtpl->text( 'main' );
	}
}

if( defined( 'NV_SYSTEM' ) )
{
	$content = nv_fanpage( $block_config );
}