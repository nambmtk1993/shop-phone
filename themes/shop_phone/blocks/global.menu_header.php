<?php

/**
 * @Project NUKEVIET 4.x
 * @Author Mr.Thinh (thinhwebhp@gmail.com)
 * @Copyright (C) 2016 Thinhweb Blog. All rights reserved
 * @License GNU/GPL version 2 or any later version
 * @Createdate Thu,17 Apr 2016 04:03:46 GMT
 */

if( ! defined( 'NV_MAINFILE' ) ) die( 'Stop!!!' );

if( ! nv_function_exists( 'nv_menu_header' ) )
{
	/**
	 * nv_menu_header_config
	 */

	 function nv_menu_header_config( $module, $data_block, $lang_block )
	 {
	 	global $lang_block, $nv_Cache;

	 	$html = '';
		$html .= "<div class=\"form-group\">";
		$html .= "	<label class=\"control-label col-sm-6\">" . $lang_block['menuid_top'] . "</label>";
		$html .= "	<div class=\"col-sm-18\"><select name=\"menuid\" class=\"w300 form-control\">\n";

		$sql = "SELECT * FROM " . NV_PREFIXLANG . "_menu ORDER BY id DESC";
		$list = $nv_Cache->db( $sql, 'id', $module );
		foreach( $list as $l )
		{
			$sel = ( $data_block['menuid'] == $l['id'] ) ? ' selected' : '';
			$html .= "<option value=\"" . $l['id'] . "\" " . $sel . ">" . $l['title'] . "</option>\n";
		}

		$html .= "	</select></div>\n";
		$html .= "</div>";

		$html .= "<div class=\"form-group\">";
		$html .= "<label class=\"control-label col-sm-6\">";
		$html .= $lang_block['title_length'];
		$html .= "</label>";
		$html .= "<div class=\"col-sm-18\">";
		$html .= "<input type=\"text\" class=\"form-control w100\" name=\"config_title_length\" value=\"" . $data_block['title_length'] . "\"/>";
		$html .= "</div>";
		$html .= "</div>";

		return $html;
	 }

	/**
	 * nv_menu_header_submit
	 */

	function nv_menu_header_submit( $module, $lang_block )
	{
		global $nv_Request;
		$return = array();
		$return['error'] = array();
		$return['config'] = array();
		$return['config']['menuid'] = $nv_Request->get_int( 'menuid', 'post', 0 );
		$return['config']['menuid2'] = $nv_Request->get_int( 'menuid2', 'post', 0 );
		$return['config']['title_length'] = $nv_Request->get_int( 'config_title_length', 'post', 24 );
		return $return;
	}


	/**
	 * nv_menu_header()
	 *
	 * @param mixed $block_config
	 * @return
	 */
	function nv_menu_header( $block_config )
	{
		global $db, $db_config, $global_config, $site_mods, $module_info, $nv_Cache, $module_name, $module_file, $module_data, $lang_global, $catid, $home;

		if( file_exists( NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/blocks/global.menu_header.tpl' ) )
		{
			$block_theme = $global_config['module_theme'];
		}
		elseif( file_exists( NV_ROOTDIR . '/themes/' . $global_config['site_theme'] . '/blocks/global.menu_header.tpl' ) )
		{
			$block_theme = $global_config['site_theme'];
		}
		else
		{
			$block_theme = 'default';
		}

		$array_menu = array();

		$sql = 'SELECT id, parentid, title, link, icon, note, subitem, groups_view, module_name, op, target, css, active_type FROM ' . NV_PREFIXLANG . '_menu_rows WHERE status=1 AND mid = ' . $block_config['menuid'] . ' ORDER BY weight ASC';

		$list = $nv_Cache->db( $sql, '', 'menu' );
		foreach( $list as $row )
		{
			if( nv_user_in_groups( $row['groups_view'] ) )
			{
				switch( $row['target'] )
				{
					case 1:
						$row['target'] = '';
						break;
					case 3:
						$row['target'] = ' onclick="window.open(this.href,\'targetWindow\',\'toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,\');return false;"';
						break;
					default:
						$row['target'] = ' onclick="this.target=\'_blank\'"';
				}
				if( ! empty( $row['icon'] ) and file_exists( NV_UPLOADS_REAL_DIR . '/menu/' . $row['icon'] ) )
				{
					$row['icon'] = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/menu/' . $row['icon'];
				}
				else
				{
					$row['icon'] = '';
				}
				$array_menu[$row['parentid']][$row['id']] = array(
					'id' => $row['id'],
					'title' => $row['title'],
					'title_trim' =>  $row['title'], $block_config['title_length'],
					'target' => $row['target'],
					'note' => empty( $row['note'] ) ? $row['title'] : $row['note'],
					'link' => nv_url_rewrite( nv_unhtmlspecialchars( $row['link'] ), true ),
					'icon' => $row['icon'],
					'css' => $row['css'],
					'active_type' => $row['active_type'],
				);
			}
		}

		$xtpl = new XTemplate( 'global.menu_header.tpl', NV_ROOTDIR . '/themes/' . $block_theme . '/blocks' );
		$xtpl->assign( 'LANG', $lang_global );
		$xtpl->assign( 'NV_BASE_SITEURL', NV_BASE_SITEURL );
		$xtpl->assign( 'BLOCK_THEME', $block_theme );
		$xtpl->assign( 'THEME_SITE_HREF', NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA );
		$xtpl->assign( 'TEMPLATE', $global_config['module_theme'] );
		
		$xtpl->assign('DATA', $block_config);

		foreach( $array_menu[0] as $id => $item )
		{
			$xtpl->assign( 'TOP_MENU', $item );
			if( ! empty( $item['icon'] ) )
			{
				$xtpl->parse( 'main.top_menu.icon' );
			}
			$xtpl->parse( 'main.top_menu' );
		}

		$xtpl->parse( 'main' );
		return $xtpl->text( 'main' );
	}

}

if( defined( 'NV_SYSTEM' ) )
{
	$content = nv_menu_header( $block_config );
}