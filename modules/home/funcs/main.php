<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2017 VINADES.,JSC. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Thu, 02 Feb 2017 08:48:59 GMT
 */

if (!defined('NV_IS_MOD_HOME')) die('Stop!!!');

$page_title = $module_info['custom_title'];
$key_words = $module_info['keywords'];

/*Shops*/
$module = 'shops';
// Categories
global $global_array_shops_cat;
$sql = 'SELECT catid, parentid, lev, ' . NV_LANG_DATA . '_title AS title, ' . NV_LANG_DATA . '_title_custom AS title_custom, ' . NV_LANG_DATA . '_alias AS alias, viewcat, numsubcat, subcatid, newday, typeprice, form, group_price, viewdescriptionhtml, numlinks, ' . NV_LANG_DATA . '_description AS description, ' . NV_LANG_DATA . '_descriptionhtml AS descriptionhtml, inhome, ' . NV_LANG_DATA . '_keywords AS keywords, ' . NV_LANG_DATA . '_tag_description AS tag_description, groups_view, cat_allow_point, cat_number_point, cat_number_product, image FROM ' . $db_config['prefix'] . '_' . $module . '_catalogs ORDER BY sort ASC';
$global_array_shops_cat = $nv_Cache->db($sql, 'catid', $module);

// Groups
global $global_array_group;
$sql = 'SELECT groupid, parentid, lev, ' . NV_LANG_DATA . '_title AS title, ' . NV_LANG_DATA . '_alias AS alias, viewgroup, numsubgroup, subgroupid, ' . NV_LANG_DATA . '_description AS description, inhome, indetail, in_order, ' . NV_LANG_DATA . '_keywords AS keywords, numpro, image, is_require FROM ' . $db_config['prefix'] . '_' . $module . '_group ORDER BY sort ASC';
$global_array_group = $nv_Cache->db($sql, 'groupid', $module);

// Lay ty gia ngoai te
$pro_config = $module_config[$module];

if (!empty($pro_config)) {
    $temp = explode('x', $pro_config['image_size']);
    $pro_config['homewidth'] = $temp[0];
    $pro_config['homeheight'] = $temp[1];
    $pro_config['blockwidth'] = $temp[0];
    $pro_config['blockheight'] = $temp[1];
}

$sql = 'SELECT code, currency,symbol, exchange, round, number_format FROM ' . $db_config['prefix'] . '_' . $module . '_money_' . NV_LANG_DATA;
$cache_file = NV_LANG_DATA . '_' . md5($sql) . '_' . NV_CACHE_PREFIX . '.cache';
if (($cache = $nv_Cache->getItem($module, $cache_file)) != false) {
    $money_config = unserialize($cache);
} else {
    $money_config = array();
    $result = $db->query($sql);
    while ($row = $result->fetch()) {
        $money_config[$row['code']] = array(
            'code' => $row['code'],
            'currency' => $row['currency'],
            'symbol' => $row['symbol'],
            'exchange' => $row['exchange'],
            'round' => $row['round'],
            'number_format' => $row['number_format'],
            'decimals' => $row['round'] > 1 ? $row['round'] : strlen($row['round']) - 2,
            'is_config' => ($row['code'] == $pro_config['money_unit']) ? 1 : 0
        );
    }
    $result->closeCursor();
    
    $cache = serialize($money_config);
    $nv_Cache->setItem($module, $cache_file, $cache);
}

// Lay Giam Gia
$sql = 'SELECT did, title, begin_time, end_time, config FROM ' . $db_config['prefix'] . '_' . $module . '_discounts';
$cache_file = NV_LANG_DATA . '_' . md5($sql) . '_' . NV_CACHE_PREFIX . '.cache';
if (($cache = $nv_Cache->getItem($module, $cache_file)) != false) {
    $discounts_config = unserialize($cache);
} else {
    $discounts_config = array();
    $result = $db->query($sql);
    while ($row = $result->fetch()) {
        $discounts_config[$row['did']] = array(
            'title' => $row['title'],
            'begin_time' => $row['begin_time'],
            'end_time' => $row['end_time'],
            'config' => unserialize($row['config'])
        );
    }
    $result->closeCursor();
    
    $cache = serialize($discounts_config);
    $nv_Cache->setItem($module, $cache_file, $cache);
}

//Tình trạng hàng
global $arr_status_product;
$arr_status_product = array(
    '0' => array(
        'id' => '0',
        'name' => $lang_module['empty_product']
    ),
    '1' => array(
        'id' => '1',
        'name' => $lang_module['isset_product']
    ),
    '2' => array(
        'id' => '2',
        'name' => $lang_module['near_product']
    )
);

foreach ($global_array_shops_cat as $catid_i => $array_info_i) {
    if ($array_info_i['parentid'] == 0 and $array_info_i['inhome'] != 0) {
        $array_cat = array();
        $array_cat = GetCatidInParent($catid_i, true);
        
        // Fetch Limit
        $db->sqlreset()->select('COUNT(*)')->from($db_config['prefix'] . '_' . $module . '_rows t1')->where('listcatid IN (' . implode(',', $array_cat) . ') AND inhome=1 AND status =1');
        
        $num_pro = $db->query($db->sql())->fetchColumn();
        
        $db->select('id, listcatid, publtime, ' . NV_LANG_DATA . '_title, ' . NV_LANG_DATA . '_alias, ' . NV_LANG_DATA . '_hometext, homeimgalt, homeimgfile, homeimgthumb, product_code, product_number, product_price, money_unit, discount_id, showprice, ' . NV_LANG_DATA . '_gift_content, gift_from, gift_to')
        ->order('id DESC')
        ->limit(8);
        
        $result = $db->query($db->sql());
        $data_pro = array();
        
        while (list($id, $listcatid, $publtime, $title, $alias, $hometext, $homeimgalt, $homeimgfile, $homeimgthumb, $product_code, $product_number, $product_price, $money_unit, $discount_id, $showprice, $gift_content, $gift_from, $gift_to) = $result->fetch(3)) {
            if ($homeimgthumb == 1) {
                //image thumb
                
                $thumb = NV_BASE_SITEURL . NV_FILES_DIR . '/' . $module . '/' . $homeimgfile;
            } elseif ($homeimgthumb == 2) {
                //image file
                
                $thumb = NV_BASE_SITEURL . NV_UPLOADS_DIR . '/' . $module . '/' . $homeimgfile;
            } elseif ($homeimgthumb == 3) {
                //image url
                
                $thumb = $homeimgfile;
            } else {
                //no image
                
                $thumb = NV_BASE_SITEURL . 'themes/' . $module_info['template'] . '/images/' . $module_file . '/no-image.jpg';
            }
            
            $data_pro[] = array(
                'id' => $id,
                'listcatid' => $listcatid,
                'publtime' => $publtime,
                'title' => $title,
                'alias' => $alias,
                'hometext' => $hometext,
                'homeimgalt' => $homeimgalt,
                'homeimgthumb' => $thumb,
                'product_code' => $product_code,
                'product_number' => $product_number,
                'product_price' => $product_price,
                'discount_id' => $discount_id,
                'money_unit' => $money_unit,
                'showprice' => $showprice,
                'gift_content' => $gift_content,
                'gift_from' => $gift_from,
                'gift_to' => $gift_to,
                'newday' => $global_array_shops_cat[$listcatid]['newday'],
                'link_pro' => NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module_name . '&amp;' . NV_OP_VARIABLE . '=' . $global_array_shops_cat[$catid_i]['alias'] . '/' . $alias . $global_config['rewrite_exturl'],
                'link_order' => NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module_name . '&amp;' . NV_OP_VARIABLE . '=setcart&amp;id=' . $id
            );
        }
        
        $data_content[] = array(
            'catid' => $catid_i,
            'subcatid' => $array_info_i['subcatid'],
            'title' => $array_info_i['title'],
            'link' => $array_info_i['link'],
            'data' => $data_pro,
            'num_pro' => $num_pro,
            'num_link' => $array_info_i['numlinks']
        );
    }
}

$contents = nv_theme_home_main($data_content);

include NV_ROOTDIR . '/includes/header.php';
echo nv_site_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';