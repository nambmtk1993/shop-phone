<?php

/**
 * @Project NUKEVIET 4.x
 * @Author VINADES.,JSC (contact@vinades.vn)
 * @Copyright (C) 2017 VINADES.,JSC. All rights reserved
 * @License: Not free read more http://nukeviet.vn/vi/store/modules/nvtools/
 * @Createdate Thu, 02 Feb 2017 08:48:59 GMT
 */
if (! defined ( 'NV_IS_MOD_HOME' ))
	die ( 'Stop!!!' );

/**
 * nv_theme_home_main()
 *
 * @param mixed $array_data
 * @return
 */
function nv_theme_home_main($list_home_shops) {
	global $global_config, $lang_module, $op, $module_file, $module_config, $module_info, $db, $db_config, $nv_Cache, $site_mods, $global_array_shops_cat, $global_array_group;
	$xtpl = new XTemplate ( $op . '.tpl', NV_ROOTDIR . '/themes/' . $module_info ['template'] . '/modules/' . $module_file );
	$xtpl->assign ( 'LANG', $lang_module );
	$xtpl->assign ( 'THEMES', $module_info ['template'] );

	/* view shops */
	$link_shops = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module;
	$xtpl->assign ( 'LINKSHOPS', $link_shops );
	$pro_config = $module_config [$module];
	// view home
	nv_view_product_home_main ( $list_home_shops, $xtpl, 'homepro' );

	if (! defined ( 'MODAL_LOADED' )) {
		$xtpl->parse ( 'main.modal_loaded' );
		define ( 'MODAL_LOADED', true );
	}
	/* end view shops */

	$xtpl->parse ( 'main' );
	return $xtpl->text ( 'main' );
}

function nv_view_product_home_main($data_content, $xtpl, $pare) {
    global $global_config, $module, $pro_config, $module_file, $lang_module, $module_config, $module_info, $op, $db, $db_config, $nv_Cache, $site_mods, $global_array_shops_cat, $global_array_group, $pro_config, $arr_status_product;

	$link = NV_BASE_SITEURL . 'index.php?' . NV_LANG_VARIABLE . '=' . NV_LANG_DATA . '&amp;' . NV_NAME_VARIABLE . '=' . $module . '&amp;' . NV_OP_VARIABLE . '=';
	
	if (!empty($data_content)) {
	    $i = 1;
	    foreach ($data_content as $data_row) {
	        if ($data_row['num_pro'] > 0) {
	            $xtpl->assign('TITLE_CATALOG', $data_row['title']);
	            $xtpl->assign('LINK_CATALOG', $data_row['link']);
	            $xtpl->assign('NUM_PRO', $data_row['num_pro']);
	            $num_row = 24 / $pro_config['per_row'];
	            foreach ($data_row['data'] as $data_row_i) {
	                
	                $xtpl->assign ( 'id', $data_row_i ['id'] );
	                $xtpl->assign ( 'link', $link . $global_array_shops_cat [$data_row_i ['listcatid']] ['alias'] . '/' . $data_row_i ['alias'] . $global_config ['rewrite_exturl'] );
	                $xtpl->assign ( 'title', $data_row_i ['title'] );
	                $xtpl->assign ( 'src_img', $data_row_i['homeimgthumb'] );
	                $xtpl->assign ( 'time', nv_date ( 'd-m-Y h:i:s A', $data_row_i ['addtime'] ) );
	                $xtpl->assign('height', $pro_config['homeheight']);
	                $xtpl->assign('width', $pro_config['homewidth']);
	                $xtpl->assign('TITLE0', nv_clean60($data_row_i['title'], 40));
	                
	                if ($pro_config ['active_order'] == '1' and $pro_config ['active_order_non_detail'] == '1') {
	                    if ($data_row_i ['showprice'] == '1') {
	                        if ($data_row_i ['product_number'] > 0 or $pro_config ['active_order_number']) {
	                            // Kiem tra nhom bat buoc chon khi dat hang
	                            $listgroupid = GetGroupID ( $data_row_i ['id'] );
	                            $group_requie = 0;
	                            if (! empty ( $listgroupid ) and ! empty ( $global_array_group )) {
	                                foreach ( $global_array_group as $groupinfo ) {
	                                    if ($groupinfo ['in_order']) {
	                                        $group_requie = 1;
	                                        break;
	                                    }
	                                }
	                            }
	                            $group_requie = $pro_config ['active_order_popup'] ? 1 : $group_requie;
	                            $xtpl->assign ( 'GROUP_REQUIE', $group_requie );
	                            
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.order' );
	                        } else {
	                            // die('pass');
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.product_empty' );
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.product_empty_button' );
	                        }
	                    }
	                }
	                $price = '';
	                if ($pro_config ['active_price'] == '1') {
	                    if ($data_row_i ['showprice'] == '1') {
	                        $price = nv_get_price_tmp ( $module, $data_row_i ['id'] );
	                        if ($price['discount_unit'] != "%") {
	                            $price['discount_unit'] = $price['unit'];
	                        }
	                        $xtpl->assign ( 'PRICE', $price );
	                        if ($data_row_i ['product_price'] == 0) {
	                            $xtpl->assign ( 'DISABLED_BTN', 'disabled' );
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.price.contactprice' );
	                        } else {
	                            $xtpl->assign ( 'DISABLED_BTN', '' );
	                            if ($data_row_i ['discount_id'] and $price ['discount_percent'] > 0) {
	                                $xtpl->parse ( 'main.catalogs.loop' . $pare . '.price.nocontactprice.newprice' );
	                            } else {
	                                $xtpl->parse ( 'main.catalogs.loop' . $pare . '.price.nocontactprice.no_discounts' );
	                            }
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.price.nocontactprice' );
	                        }
	                        $xtpl->parse ( 'main.catalogs.loop' . $pare . '.price' );
	                    } else {
	                        $xtpl->parse ( 'main.catalogs.loop' . $pare . '.contact' );
	                    }
	                }
	                ;
	                if (! empty ( $price )) {
	                    if ($data_row_i ['discount_id'] and $price ['discount_percent'] > 0 and $data_row_i ['showprice']) {
	                        $xtpl->parse ( 'main.catalogs.loop' . $pare . '.discounts' );
	                    }
	                }
	                // Tình trạng hàng
	                
	                if ($pro_config ['active_order'] == '1' and $pro_config ['active_order_non_detail'] == '1') {
	                    if ($data_row_i ['showprice'] == '1') {
	                        $number_produce = ($data_row_i ['product_number'] == $arr_status_product [$data_row_i ['product_number']] ['id']) ? $arr_status_product [$data_row_i ['product_number']] ['name'] : '';
	                        $xtpl->assign ( 'STATUS_PRODUCE', $number_produce );
	                        if ($data_row_i ['product_number'] > 0) {
	                            // Kiem tra nhom bat buoc chon khi dat hang
	                            $listgroupid = GetGroupID ( $data_row_i ['id'] );
	                            $group_requie = 0;
	                            if (! empty ( $listgroupid ) and ! empty ( $global_array_group )) {
	                                foreach ( $global_array_group as $groupinfo ) {
	                                    if ($groupinfo ['in_order']) {
	                                        $group_requie = 1;
	                                        break;
	                                    }
	                                }
	                            }
	                            $group_requie = $pro_config ['active_order_popup'] ? 1 : $group_requie;
	                            $xtpl->assign ( 'GROUP_REQUIE', $group_requie );
	                            
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.order' );
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.product_order' );
	                        } else {
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.product_empty' );
	                            $xtpl->parse ( 'main.catalogs.loop' . $pare . '.product_empty_lang' );
	                        }
	                    }
	                }
	                $xtpl->parse ( 'main.catalogs.loop' . $pare );
	            }
	            if (!empty($data_row['subcatid'])) {
	                $data_row['subcatid'] = explode(',', $data_row['subcatid']);
	                foreach ($data_row['subcatid'] as $subcatid) {
	                    $items = $global_array_shops_cat[$subcatid];
	                    if ($items['inhome']) {
	                        $xtpl->assign('SUBCAT', $global_array_shops_cat[$subcatid]);
	                        $xtpl->parse('main.catalogs.subcatloop');
	                    }
	                }
	            }
	            
	            $i++;
	            $xtpl->assign('i', $i);
	            
	            $xtpl->parse('main.catalogs');
	        }
	        
	    }
	}
}

if (! nv_function_exists ( 'nv_get_price_tmp' )) {
	function nv_get_price_tmp($module_name, $pro_id) {
		global $nv_Cache, $db, $db_config, $module_config, $discounts_config;

		$price = array ();
		$pro_config = $module_config [$module_name];

		// /require_once NV_ROOTDIR . '/modules/' . $module_file . '/site.functions.php';

		$price = nv_get_price ( $pro_id, $pro_config ['money_unit'], 1, false, $module_name );

		return $price;
	}
}

/**
 * nv_get_price()
 *
 * @param mixed $pro_id
 * @param mixed $currency_convert
 * @param mixed $number
 * @param mixed $per_pro
 * @return
 */
function nv_get_price($pro_id, $currency_convert, $number = 1, $per_pro = false, $module = '') {
	global $db, $db_config, $site_mods, $module_data, $global_array_shops_cat, $pro_config, $money_config, $discounts_config;

	$return = array ();
	$discount_percent = 0;
	$discount_unit = '';
	$discount = 0;

	$module_data = ! empty ( $module ) ? $site_mods [$module] ['module_data'] : $module_data;
	$product = $db->query ( 'SELECT listcatid, product_price, money_unit, price_config, discount_id FROM ' . $db_config ['prefix'] . '_' . $module_data . '_rows WHERE id = ' . $pro_id )->fetch ();
	$price = $product ['product_price'];
	if (! $per_pro) {
		$price = $price * $number;
	}

	$r = $money_config [$product ['money_unit']] ['round'];
	$decimals = nv_get_decimals ( $currency_convert );

	if ($r > 1) {
		// $price = round( $price / $r ) * $r;
		$price = ($price / $r) * $r;
	} else {
		$price = round ( $price, $decimals );
	}
	if ($global_array_shops_cat [$product ['listcatid']] ['typeprice'] == 2) {
		$_price_config = unserialize ( $product ['price_config'] );
		if (! empty ( $_price_config )) {
			foreach ( $_price_config as $_p ) {
				if ($number <= $_p ['number_to']) {
					$price = $_p ['price'] * (! $per_pro ? $number : 1);
					break;
				}
			}
		}
	} elseif ($global_array_shops_cat [$product ['listcatid']] ['typeprice'] == 1) {
		if (isset ( $discounts_config [$product ['discount_id']] )) {
			$_config = $discounts_config [$product ['discount_id']];
			if ($_config ['begin_time'] < NV_CURRENTTIME and ($_config ['end_time'] > NV_CURRENTTIME or empty ( $_config ['end_time'] ))) {
				foreach ( $_config ['config'] as $_d ) {
					if ($_d ['discount_from'] <= $number and $_d ['discount_to'] >= $number) {
						$discount_percent = $_d ['discount_number'];
						if ($_d ['discount_unit'] == 'p') {
							$discount_unit = '%';
							$discount = ($price * ($discount_percent / 100));
						} else {
							$discount_unit = ' ' . $pro_config ['money_unit'];
							$discount = $discount_percent * $number;
						}
						break;
					}
				}
			}
		}
	}

	$price = nv_currency_conversion ( $price, $product ['money_unit'], $currency_convert, $module );

	$return ['price'] = $price;
	// Giá sản phẩm chưa format
	$return ['price_format'] = nv_number_format ( $price, $decimals, $money_config );
	// Giá sản phẩm đã format

	$return ['discount'] = $discount;
	// Số tiền giảm giá sản phẩm chưa format
	$return ['discount_format'] = nv_number_format ( $discount, $decimals, $money_config );
	// Số tiền giảm giá sản phẩm đã format
	$return ['discount_percent'] = $discount_unit == '%' ? $discount_percent : nv_number_format ( $discount_percent, $decimals, $money_config );
	// Giảm giá theo phần trăm
	$return ['discount_unit'] = $discount_unit;
	// Đơn vị giảm giá

	$return ['sale'] = $price - $discount;
	// Giá bán thực tế của sản phẩm
	$return ['sale_format'] = nv_number_format ( $return ['sale'], $decimals, $money_config );
	// Giá bán thực tế của sản phẩm đã format
	$return ['unit'] = $money_config [$currency_convert] ['symbol'];

	return $return;
}

/**
 * nv_get_decimals()
 *
 * @param mixed $currency_convert
 * @return
 */
function nv_get_decimals($currency_convert) {
	global $money_config;
	$r = $money_config [$currency_convert] ['round'];
	$decimals = 0;

	if ($r <= 1) {
		$decimals = $money_config [$currency_convert] ['decimals'];
	}
	return $decimals;
}

/**
 * nv_currency_conversion()
 *
 * @param mixed $price
 * @param mixed $currency_curent
 * @param mixed $currency_convert
 * @return
 */
function nv_currency_conversion($price, $currency_curent, $currency_convert, $module) {
	global $module_config, $discounts_config, $money_config;
	$pro_config = $module_config [$module];

	if ($currency_curent == $pro_config ['money_unit']) {
		$price = $price / $money_config [$currency_convert] ['exchange'];
	} elseif ($currency_convert == $pro_config ['money_unit']) {
		$price = $price * $money_config [$currency_curent] ['exchange'];
	}

	return $price;
}

/**
 * nv_number_format()
 *
 * @param mixed $number
 * @param integer $decimals
 * @return
 */
function nv_number_format($number, $decimals = 0) {
	global $module_config, $money_config;
	$module = 'shops';
	$pro_config = $module_config [$module];
	$number_format = explode ( '||', $money_config [$pro_config ['money_unit']] ['number_format'] );
	$str = number_format ( $number, $decimals, $number_format [0], $number_format [1] );

	return $str;
}

/**
 * GetGroupID()
 *
 * @param mixed $pro_id
 * @return
 */
function GetGroupID($pro_id, $group_by_parent = 0) {
	global $db, $db_config, $module, $global_array_group;

	$data = array ();
	$result = $db->query ( 'SELECT group_id FROM ' . $db_config ['prefix'] . '_' . $module . '_group_items where pro_id=' . $pro_id );
	while ( $row = $result->fetch () ) {
		if ($group_by_parent) {
			$parentid = $global_array_group [$row ['group_id']] ['parentid'];
			$data [$parentid] [] = $row ['group_id'];
		} else {
			$data [] = $row ['group_id'];
		}
	}
	return $data;
}
/**
 * GetCatidInParent()
 *
 * @param mixed $catid
 * @param integer $check_inhome
 * @return
 */
function GetCatidInParent($catid, $check_inhome = 0)
{
    global $global_array_shops_cat, $array_cat;
    $array_cat[] = $catid;
    $subcatid = explode(',', $global_array_shops_cat[$catid]['subcatid']);
    if (!empty($subcatid)) {
        foreach ($subcatid as $id) {
            if ($id > 0) {
                if ($global_array_shops_cat[$id]['numsubcat'] == 0) {
                    if (!$check_inhome or ($check_inhome and $global_array_shops_cat[$id]['inhome'] == 1)) {
                        $array_cat[] = $id;
                    }
                } else {
                    $array_cat_temp = GetCatidInParent($id, $check_inhome);
                    foreach ($array_cat_temp as $catid_i) {
                        if (!$check_inhome or ($check_inhome and $global_array_shops_cat[$catid_i]['inhome'] == 1)) {
                            $array_cat[] = $catid_i;
                        }
                    }
                }
            }
        }
    }
    return array_unique($array_cat);
}